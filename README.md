![Chat App with React & AWS](header.jpg)

## Real Time Chat with React, GraphQL, and AWS AppSync

This application can be deployed with either AWS Amplify or CDK.

1. Start the app

```sh
npm start
```
